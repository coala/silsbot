#!/usr/bin/env python3

import os

from wit import Wit
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from gitterest import Gitter

WIT_TOKEN = os.environ.get('WIT_TOKEN')
GITTER_TOKEN = os.environ.get('GITTER_TOKEN')
GITTER_ROOM = os.environ.get('GITTER_ROOM')
client = Wit(access_token=WIT_TOKEN)

# Flask setup - for the db
app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
migrate = Migrate(app, db)


def handle_message(message):
    pass


def main():
    gitter = Gitter(GITTER_TOKEN)
    for message in gitter.roomStream(GITTER_ROOM):
        handle_message(message)


if __name__ == '__main__':
    main()
